from django.db import models


# Create your models here.

class Cat(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=100)
    age = models.IntegerField()

    class Meta:
        ordering = ('created',)
