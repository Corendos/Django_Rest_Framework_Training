from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from . import views

urlpatterns = [
    path('cats/', views.CatList.as_view()),
    path('cats/<int:pk>', views.CatDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
